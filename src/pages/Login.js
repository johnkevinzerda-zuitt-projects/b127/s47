import {Fragment, useEffect, useState} from 'react';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function Login () {

	const [email, setEmail] = useState('');
	const [password,setPassword] =useState('');
	const [isActive,setIsActive] = useState(false);

	useEffect(() => {
		if (email!==''&& password!=='') {
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[email,password]);

	let loginUser = e => {
		e.preventDefault();
		setEmail('');
		setPassword('');

		Swal.fire(
		{
			title: 'Login successful!',
			icon: 'success',
			text: 'Please wait'
		}
		)
	}

	return(
		<Fragment>
		<Form onSubmit ={e=>loginUser(e)}>
		<h1>Login</h1>
		<Form.Group>
		<Form.Label>Email address:</Form.Label>
		<Form.Control 
		type="email"
		placeholder="Enter email"
		value={email}
		onChange= {e=>setEmail(e.target.value)}
		required
		/>
		<Form.Label>Password:</Form.Label>
		<Form.Control 
		type="password"
		placeholder="Enter password"
		value={password}
		onChange= {e=>setPassword(e.target.value)}
		required
		/>
		</Form.Group>
		{isActive ?
			<Button type="submit" variant="success" id="loginBtn">Login</Button>
			:
			<Button type="submit" variant="success" id="loginBtn" disabled>Login</Button>
		}

		</Form>
		</Fragment>
		)


}